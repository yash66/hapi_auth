const {sequelize,User} = require('../models')
const Bcrypt = require('bcrypt')

exports.list = (req, h) => {
    return User.findAll().then((users) => {
  
      return { users: users };
  
    }).catch((err) => {
  
      return { err: err };
  
    });
  }


  exports.create = async (req, h) => {

    const salt = await Bcrypt.genSalt();
    const password = await Bcrypt.hash(req.payload.password,salt);
    const userData = {
      username: req.payload.username,
      password: password,
    };

    const existingUsername = await User.findOne({ where: { username : userData.username } });
    if(!existingUsername){
    return User.create(userData).then((users) => {
  
       return { message: "User Created successfully", users: users };
  
    }).catch((err) => {
  
      return { err: err };
  
    });}
    else{
        return { message: "User Already exists"};
    }
  }


 