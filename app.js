'use strict';

const Hapi = require('@hapi/hapi');
const { request } = require('http');
const path = require('path');
const Connection = require ('./dbconfig')
const {sequelize,User} = require('./models')
const UserController = require('./controllers/user')
const Bcrypt = require('bcrypt')

const JWT         = require('jsonwebtoken');
const hapiAuthJWT = require('hapi-auth-jwt2');

const fs = require('fs');


//Initializing the secret key
const secret = 'NeverShareYourSecret';

//Init Server

const init = async () => {
const server = Hapi.Server({
    host: 'localhost',
    port: '8000'
});


//Validate function for server authentication
const validate = async function (decoded, request, h) {
    const jwt_user = await User.findOne({ where: { username : decoded.username } });
    // do your checks to see if the person is valid
    if (!jwt_user) {
        console.log(" - - - - - - - decoded token:");
        console.log(decoded);
        
        console.log(User[decoded.username]);
      return { isValid: false };
    }
    else {
      return { isValid: true };
    }
};

await server.register(hapiAuthJWT);

//Server Auth stratergy
server.auth.strategy('jwt', 'jwt',
  { key: secret, // Never Share your secret key
    validate  // validate function defined above
  });
  

  

//Registering Hapi Server plugins
await server.register([
    {
        plugin: require('@hapi/vision')
    }
]);

server.views({
    engines:{
        html: require('handlebars')
    },
    path:path.join(__dirname, 'views')
});

server.route([

{
  //Home page
    method: 'GET',
    path: '/',
    handler: (request,h) =>{
        return "<h1>Hello World</h1>";
    },
    config: { auth: 'jwt' }

},

//Test Method display
{
    method: 'GET',
    path: '/display',
    handler: UserController.list,
    config: { auth: 'jwt' }

},


//All the vision routes
{
    method: 'GET',
    path: '/login',
    handler: (request,h) => {
        return h.view('login');
    }
},

{
    method: 'GET',
    path: '/signup',
    handler: (request,h) => {
        return h.view('signup');
    }
},

{
    method: 'GET',
    path: '/changepass',
    handler: (request,h) => {
        return h.view('password');
    },
    config: { auth: false }
},



//Authentication post Routes

{
    method: 'POST',
    path: '/signuppost',
    handler: UserController.create,
    config: { auth: false }
},

{
    method: 'POST',
    path: '/changepasspost',
    handler: async (req,h) => {
        const username = req.payload.username;
        const password = req.payload.password;
        const user = {username: username};
        
        const login_user = await User.findOne({ where: { username : username } });
        const match = await Bcrypt.compare(password, login_user.password);
          if (match){
            if(req.payload.new_password1 === req.payload.new_password2){
              //Replace Existing password
              await User.update({ password: req.payload.new_password1 }, {
                where: {
                  username: username
                }
              });
              return { message: "Passwords Changed successfully"};
          }
          }
          else{
            return { message: "Passwords Dont Match"};
              }
        },
    config: { auth: false }
},



// {
//     method: 'POST',
//     path: '/loginpost',
//     config: { auth: false },
//     handler: async (req,h) => {
//     const username = req.payload.username;
//     const password = req.payload.password;
//     const user = {username: username}
    
//     const login_user = await User.findOne({ where: { username : username } });
//     if (login_user.password === password){
//       //success
//       return JWT.sign(user,secret);
//     //   return { isAuthenticated: true};
//     }
//     else{
//       // faliure
//     //   return { isAuthenticated: false};
//     return { message: "Invalid Credentials"};
//     }
//     },
//     config: { auth: false }
// },

{
    method: 'POST',
    path: '/loginpost',
    config: { auth: false },
    handler: async (req,h) => {
    const username = req.payload.username;
    const password = req.payload.password;
    const user = {username: username}
    
    const login_user = await User.findOne({ where: { username : username } });

    const match = await Bcrypt.compare(password, login_user.password);
    if (match){
      const login_jwt =  { jwt : JWT.sign(user,secret)};
      return login_jwt;
    }
    else{
      return { message: "Invalid Credentials"};
        }
    },
    config: { auth: false }
},



//Temporary route for testing
{
    method: 'GET',
    path: '/put',
    handler: async (request,h) => {
        const username = 'Yash';
        const password = 'Test';
        const pushUsers = await Connection.putUsers(username,password);
        console.log(pushUsers);
        return ('HI');
    }
},

//using wildcard route to handle 404 errors
{
    method: 'GET',
    path: '/{any*}',
    handler: (request,h) =>{
        return `Invalid url, does not exist`;
    }
},

//Image upload
//Implementation Pending
{
    method: 'GET',
    path: '/image',
    handler: async (request,h) => {
        return h.view('image');
    },
    config: { auth: false }
},

{
    method: 'POST',
    path: '/imagepost',
    handler: async (req,h) => {
        try {
        console.log(req.payload);
        const data = request.payload;

        const { imageUrl } = await handleFileUpload(data);
        // const user = new User ({ image: imageUrl });
        // const result = user.save();
        return ({ message: 'Saved Successfully' });
      } catch (err) {
        throw err
      }},
    config: { auth: false }
},


]);


// const handleFileUpload = file => {
//     return new Promise((resolve, reject) => {
//       fs.writeFile('./upload/test.png', file, err => {
//          if (err) {
//           reject(err)
//          }
//          resolve({ message: 'Upload successfully!' })
//       })
//     })
//    }



   const handleFileUpload = file => {
    return new Promise((resolve, reject) => {
      // const filename = file.hapi.filename;
      const filename = file;
      const data = file._data;
  
      return fs.writeFile(`./uploads/${filename}`, data, err => {
        if (err) {
          return reject(err)
        }
        return resolve({
          message: "Upload successfully!",
          imageUrl: `${server.info.uri}/uploads/${filename}`
        })
      })
    })
  }

await server.start();
console.log(`Server started on : ${server.info.uri}`);

}

process.on('unhandledRejection',(err) => {
    console.log(err);
    process.exit(1);
})

init();